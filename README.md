Project Name : Postman API Testing

Summary: This project consists of  testing sample  REST APIs with Postman.

Contents:
POSTMAN
REST APIS
COLLECTIONS
VARIABLES
ENVIRONMENT
PRE TEST SCRIPTS AND TEST SCRIPTS
DATA DRIVEN TESTING
NEWMAN REPORT
INSTALLATIONS
SOURCES

1.POSTMAN
postman is a tool used for testing APIs where you send different HTTPs requests like GET, PUT, POST, PATCH, DELETE and validate the response.
We can Automate api test by using collection runner and writing tests in java script.

2.REST APIs
REST stands for Representational state transfer an architectural style followed to develop these apis , which use JSON format to exchange the data, lightweight in nature, and also supports caching which improves performance and reduce server load.
In this particular project i tested sample rest apis taken from https://reqres.in/ and performed api testing with different HTTP methods.
One is GOrest apis ,and other is reqres apis for data driven testing
In GO Rest apis i have used different methods and performed API chaining 
1. POST - used to create a user 
2. GET  - used to get the user
3. PUT/Patch - used to update or modify the user
4. Delete - used to delete the user.
IN reqres apis i taken POST method and performed DATA DRIVEN TESTING by adding JSON, .CSV files at collection runner. 

3.COLLECTIONS

- The structure of collection is it contains API http  requests, under collection we can create folders and under the folder we can create requests 
- Under collection level we can write/define  pre- tests , authentication, test scripts, and varaibles.
- We can execute all the requests under one collection using colletion runner.

4. VARIABLES

For this project i have used collection varaible at collection level and local varaibles in pre test scripts

[](https://gitlab.com/myautomation3/postmanapitesting#we-declare-variables-as-below-var_name)
we declare variables as: {{var_name}}

we can create variables for requesturls as well.

5. ENVIRONMENT:

IN real time we work on different environments example like test , production environmetns each env has different configuration setups we have t work acordinng to those envs


6. PRE TEST SCRIPTS AND TEST SCRIPTS

Pre tests: what ever we write here will execute first when we sent the request
test scripts : this will execute after getting the response.

In postman there is library called pm library we write test cases with use of chai and moca frameworks in java script
we will write all the necessary test validationn  requrired for api and automate them.

7. DATA DRIVEN TESTING .

- In this project i aslo performed data driven testing by taking smaple api from reqres with POST method
- checked random data generation for this i took random data from postman learning website where all documentationion provided to   generate random data
- taking the data from excel , created collection variables for request body and created a data in excel file and save it as  .csv and add the file in postman while running the collection runner.
- taking data from json files , created collection variabales for request body and create a data in notepad ,  save the file as .json , written pretest scripts to acces the json file, and the files a collection runner.

8. Newman

- Newman is CLI used to generate html reports of the postman collections so that we can get detailed report of the collection what we did
- we can generate two type of files one is html and htmlextra where html gives us document typpe of report and html extra gives us nice GUI actionable report in a very detailed manner.


we need to export the collection, environment files from postman save to a folder. and do the following:

- install newnam cli and run this command to generate report
- syntax: newman run {location of postman collection \ name of the collection } -d{location of the data file and name of the data file } -e {location of postman environment  \ name of the environment } --reporters {name of the reporter} --reporter-{ name of the reporter }-export {location where you want save your out put html report} 

9. INSTALLATIONS

install postman follow this link:

https://www.postman.com/downloads/

install newman

to install newman first need to install node.js follow this link for reference.

https://learning.postman.com/docs/collections/using-newman-cli/installing-running-newman/

10.Sources

for sample apis : https://reqres.in/

api : https://gorest.co.in/

for random data generation:

https://learning.postman.com/




